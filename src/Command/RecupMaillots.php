<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

class RecupMaillots extends Command
{

    /**
     * L'url de base d'un forum, pour vérification
     */
    const URL_FORUM = 'https://legruppetto.fr/forum/viewforum.php?f=';

    /**
     * Le texte demandant l'adresse du forum
     */
    const TEXT_URL = "L'adresse sur le forum où se trouvent les maillots à récupérer";

    /**
     * Le texte demandant le dossier de destination des maillots à récupérer
     */
    const TEXT_DESTINATION = "Le dossier où enregistrer les maillots récupérés";

    /**
     * Le format utilisé dans les barres de progressions
     */
    const FORMAT_PROGRESS_BARS = ' %current:2s%/%max:2s% [%bar%] %percent:3s%% [%message%]';

    /**
     * La liste des noms d'accessoires
     */
    const ACCESSOIRES
        = array('bottle', 'corsair', 'gloves', 'handles', 'leggings', 'shoes',
                'socks', 'transfert_ar', 'transfert_av');

    /**
     * Correspondance flag => nom pour les maillots de leaders des GTs
     */
    const LEADERS_GTS = array(
        't' => 'time',
        'p' => 'points',
        'm' => 'mountain',
        'y' => 'young'
    );

    /**
     * Equivalences noms de courses et nom dans le fichier
     */
    const AUTRES_COURSES = array(
        'Down Under Tour' => 'down_under',
        'Paris Nice' => 'parisnice',
        'Tirreno Adriatico' => 'tirrenoadriatico',
        'Vuelta al Pais Vasco' => 'tourpaysbasque',
        'Volta Ciclista a Catalunya' => 'tourdecatalogne',
        'Tour de Turquie' => '',
        'Tour de Romandie' => 'tourderomandie',
        'Critérium du Dauphiné' => 'dauphinelibere',
        'Tour de Suisse' => 'tourdesuisse',
        'Tour de Pologne' => 'tourpologne',
        'BinckBank Tour' => 'tourdubenelux',
        'Abu Dhabi Tour' => 'tourabudhabi',
        'Amgen Tour of California' => 'tourofcalifornia',
        'Tour of the Alps' => 'trentino',
        'Circuit de la Sarthe' => 'circuitsarthe',
        'Étoile de Besseges' => 'besseges',
        'Tour du Poitou-Charentes' => '',
        'Tour La Provence' => 'tourlaprovence',
        'Tour of Slovenia' => 'tourslovenia',
        'Volta a Portugal' => 'tourduportugal',
        'Volta ao Algarve' => 'voltaalgarve',
        'Circuit des Ardennes international' => 'circuitardennes',
        'Istrian Spring Trophy' => 'istrianspringtrophy',
        'Tour de Normandie' => 'tournormandie',
        'Tour of Mersin' => ''
    );

    /**
     * Le client permettant de naviguer et interprêter les pages du forum
     *
     * @var Client
     */
    private $client;

    /**
     * La gestion des entrées/sorties en ligne de commande
     *
     * @var SymfonyStyle
     */
    private $io;

    /**
     * L'adresse du forum où récupérer les maillots
     *
     * @var string
     */
    private $url;

    /**
     * Le dossier où enregistrer les maillots
     *
     * @var string
     */
    private $destination;

    /**
     * @var Output
     */
    private $output;

    /**
     * Le nombre d'erreurs de téléchargements d'images
     *
     * @var int
     */
    private $erreurs = array();

    /**
     * Le nombre d'images déjà existantes
     *
     * @var int
     */
    private $existants = 0;

    /**
     * Initialise la commande
     */
    protected function configure()
    {
        $this->setName('maillots:recup')
            ->setDescription('Charge les maillots')
            ->setHelp('Cette commande permet de récupérer les maillots')
            ->addArgument('url', InputArgument::OPTIONAL, self::TEXT_URL)
            ->addArgument('destination', InputArgument::OPTIONAL,
                self::TEXT_DESTINATION);

        $this->client = new Client();
    }

    /**
     * Exécution de la commande
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);
        $this->url = $input->getArgument('url');
        $this->destination = $input->getArgument('destination');

        $this->io->title('Outils de récupération de maillots et accessoires');

        $this->askUrl();
        $this->askDestination();

        $this->io->text('Récupération et analyse de la page principale');
        $crawler = $this->load($this->url);
        $sections = $crawler->filter("ul.topiclist.forums li dt a.forumtitle")
            ->each(function (Crawler $node) {
                return $node->text();
            });

        if (count($sections) == 0) {
            // Pas de sections dans la page principale donnée
            $analyser = $this->io->confirm(
                "Aucune section trouvée, analyser la page comme une section ?",
                true
            );
            if ($analyser === true) {
                $this->analyseSection($crawler);
            }

            return;
        }

        // Analyse des sections trouvées
        $this->io->section('Sections trouvées :');
        $this->io->listing($sections);

        $crawler->filter("ul.topiclist.forums li dt a.forumtitle")
            ->each(function (Crawler $node) {
                $analyser = $this->io->confirm(
                    "Analyser la section \"".$node->text()."\" ?",
                    true
                );
                if ($analyser === true) {
                    $this->analyseSection($this->load($node->attr('href')));
                }
            });
    }

    /**
     * Analyse une section du forum pour y récupérer les maillots et accessoires
     * contenus dans celle-ci
     *
     * @param Crawler $crawler le contenu de la page de la section à analyser
     */
    public function analyseSection(Crawler $crawler) {
        $this->io->text("Analyse de la section \""
            .$crawler->filter('h1')->text()."\"...");
        $nextPage = true;
        $teams = array();
        $this->erreurs = array();
        $this->existants = 0;
        while ($nextPage) {
            $teams = array_merge($teams, $crawler
                ->filter('ul.topiclist.topics li dt a.topictitle')
                ->each(function (Crawler $node) {
                    $topic = $node->text();
                    if (!preg_match("/\[([0-9a-zA-Z]{3})\]/", $topic, $matches)) {
                        return null;
                    }
                    return array($node, $matches[1]);
                }));

            // Supprime les lignes vides
            $teams = array_filter($teams);

            // Vérifie s'il y a d'autres pages dans cette section
            $currentPage = $crawler->filter('.topic-actions .pagination ' .
                'span.mobile > strong');
            if ($currentPage->count() == 0) {
                // Il n'y a qu'une page
                $nextPage = false;
                continue;
            }

            // Récupère les liens des pages suivantes
            $nextLinks = $currentPage->nextAll()->filter('a');
            if ($nextLinks->count() == 0) {
                // On est dans la dernière page de la section
                $nextPage = false;
                continue;
            }

            // Charge la page suivante
            $crawler = $this->load($nextLinks->first()->attr('href'));
        }

        // Affichage de la barre de progression globale
        $this->io->text(count($teams)." équipes trouvées dans la section");
        $progressTeams = new ProgressBar($this->output, count($teams));
        $progressTeams->setFormat(self::FORMAT_PROGRESS_BARS);
        $progressTeams->start();

        foreach ($teams as $team) {
            // Affiche le nom de l'équipe dans la progression
            $progressTeams->setMessage(strtolower($team[1]));
            $progressTeams->display();
            $this->io->newLine();

            // Récupère dans le topic les adresses des images à télécharger
            $images = $this->analyseTopic($team[0]);

            // Affichage de la barre de progression secondaire
            $progressTeam = new ProgressBar($this->output, count($images) + 1);
            $progressTeam->setFormat(self::FORMAT_PROGRESS_BARS);
            $progressTeam->start();

            foreach ($images as $image => $url) {
                $progressTeam->setMessage(strtolower($image));
                $progressTeam->display();
                $images[$image] = $this->save($team[1], $image, $url);
                $progressTeam->advance();
            }

            $progressTeam->setMessage("Création zip");
            $this->zip($team[1], $images);
            $progressTeam->advance();

            // Remonte d'une ligne pour supprimer la barre de progression
            // secondaire
            $this->output->write("\033[1A");
            $progressTeams->advance();
        }

        // Saute une ligne après les barres de progressions
        $this->io->newLine(3);

        if ($this->existants > 0) {
            $this->io->warning($this->existants . " images existaient déjà");
        }
        if (count($this->erreurs) > 0) {
            $this->io->error(count($this->erreurs)." images n'ont pas pu être "
                ."téléchargées");
            $this->io->listing($this->erreurs);
        }
    }

    /**
     * Analyse le contenu du premier post d'un topic, pour y récupérer les
     * maillots et accessoires qu'il contient
     *
     * @param Crawler $node le contenu de la page du topic
     *
     * @return array les maillots à enregistrer
     */
    public function analyseTopic(Crawler $node) {
        $crawler = $this->load($node->attr('href'));

        // Récupération du contenu du premier post
        $premierPost = $crawler->filter('.postbody > .content')->first();
        if ($premierPost->count() == 0) {
            return array();
        }

        // Note : obligé de passer par une expression régulière, les textes
        // accessoires n'étant pas dans des tags séparés, du coup autant faire
        // le reste aussi avec des expressions régulières

        $images = array();
        $html = $premierPost->html();

        // Récupération du minimaillot
        preg_match('/<img[^>]*src="([^"]+)".*Maillot de l\'équipe/',
            $html, $matches);
        if (!empty($matches[1])) {
            $images['minimaillot'] = $matches[1];
        }

        // Récupération du maillot
        preg_match('/Maillot de l\'équipe.*<img[^>]*src="([^"]+)".*Accessoires/',
            $html, $matches);
        if (!empty($matches[1])) {
            $images['maillot'] = $matches[1];
        }

        // Récupération des accessoires
        foreach (self::ACCESSOIRES as $accessoire) {
            preg_match('/'.$accessoire.'\s*:\s*<a href="([^"]+)"/i',
                $html, $matches);
            if (!empty($matches[1])) {
                $images[$accessoire] = $matches[1];
            }
        }

        // Récupération des maillots de champions
        preg_match_all('/<span class="flag flag16 f16([a-zA-Z0-9]{2,3})">'
            . '<\/span>\s*<a href="([^"]+)"/', $html, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            if ($match[1] == 'WO') {
                $match[1] = 'world';
            }
            $images['maillot_'.$match[1]] = $match[2];
        }

        // Récupération des maillots de leader des GT
        preg_match_all('/<span class="flag flag16 f16(giro|tour|vuelta)_'
                .'([a-zA-Z])"><\/span>\s*<a href="([^"]+)"/', $html, $matches,
            PREG_SET_ORDER);
        $types = self::LEADERS_GTS;
        foreach ($matches as $match) {
            $type = isset($types[$match[2]]) ? $types[$match[2]] : $match[2];
            $images['maillot_'.$type.'_'.$match[1]] = $match[3];
        }

        // Récupération des maillots de leader des autres courses

        return $images;
    }

    /**
     * Demande quelle est l'url de la page principale où se trouve les maillots
     * à récupérer
     */
    private function askUrl() {
        if (!empty($this->url) && self::isUrlValide($this->url)) {
            return;
        }

        $this->url = $this->io->ask(self::TEXT_URL, self::URL_FORUM.'137',
            function ($url) {
                if (!self::isUrlValide($url)) {
                    throw new \RuntimeException(
                        "L'adresse entrée n'est pas valide");
                }
                return $url;
            }
        );
    }

    /**
     * Teste si l'url entrée correspond à une url du forum
     *
     * @param string $url l'url à tester
     *
     * @return bool
     */
    private static function isUrlValide($url) {
        return strpos($url, self::URL_FORUM) === 0;
    }

    /**
     * Demande quelle est l'url de la page principale où se trouve les maillots
     * à récupérer
     */
    private function askDestination() {
        if (empty($this->destination)
            || !self::isDestinationValide($this->destination)
        ) {
            $this->destination = $this->io->ask(self::TEXT_DESTINATION, null,
                function ($url) {
                    if (!self::isDestinationValide($url)) {
                        throw new \RuntimeException(
                            "Le dossier de destination entré n'est pas valide"
                        );
                    }

                    return $url;
                }
            );
        }

        if (!in_array($this->destination[strlen($this->destination) - 1],
            array('/', '\\'))
        ) {
            $this->destination .= '/';
        }
    }

    /**
     * Teste si le dossier de destination existe
     *
     * @param string $destination le dossier à tester
     *
     * @return bool
     */
    private static function isDestinationValide($destination) {
        return file_exists($destination);
    }

    /**
     * Charge une page et retourne l'objet permettant de l'analyser
     *
     * @param string $url l'url à charger
     *
     * @return Crawler
     */
    private function load($url) {
        $crawler = $this->client->request('GET', $url);

        return $crawler;
    }

    /**
     * Enregistre un maillot ou accessoire dans un fichier local
     *
     * @param string $code le code de l'équipe
     * @param string $name le nom de la version de maillot
     * @param string $url  l'adresse où se trouve le maillot à télécharger
     *
     * @return string le chemin du fichier enregistré
     */
    private function save($code, $name, $url) {
        $filename = strtolower($code."/".$code."_".$name.".png");
        $destination = $this->destination."images/";

        // Création des dossiers
        if (!file_exists($destination)) {
            mkdir($destination);
        }
        if (!file_exists($destination.dirname($filename))) {
            mkdir($destination.dirname($filename));
        }

        if (file_exists($destination.$filename)) {
            // L'image existe déjà, on ne la retélécharge pas
            $this->existants++;
            return $destination.$filename;
        }

        $url = html_entity_decode($url);
        $image = null;
        $nbTentatives = 0;
        while (empty($image) && $nbTentatives < 20) {
            // Attend entre chaque chargement, pour éviter une surcharge
            usleep(100000);

            // Récupération de l'image
            $image = @file_get_contents($url);
            $nbTentatives++;
        }

        if (empty($image)) {
            $this->erreurs[] = $url;
            return null;
        }

        // Enregistrement de l'image
        file_put_contents($destination.$filename, $image);

        return $destination.$filename;
    }

    /**
     * Création ou mise à jour du zip d'une équipe
     *
     * @param string $team le nom de l'équipe
     * @param array  $images les images de l'équipe à mettre dans le zip
     */
    private function zip($team, $images) {
        $team = strtolower($team);
        $cheminZip = $this->destination."zips/teams/".$team.".zip";
        if (!file_exists(dirname(dirname($cheminZip)))) {
            mkdir(dirname(dirname($cheminZip)));
        }
        if (!file_exists(dirname($cheminZip))) {
            mkdir(dirname($cheminZip));
        }

        $zip = new \ZipArchive();
        if ($zip->open($cheminZip, \ZipArchive::CREATE) !== true) {
            $this->io->error("Impossible d'ouvrir ou créer le fichier zip "
                .$cheminZip);
            return;
        }

        // Ajout des maillots dans le zip
        foreach ($images as $image) {
            if (empty($image)) {
                continue;
            }
            $cheminDansZip = "Team/$team/".basename($image);
            if ($zip->locateName($cheminDansZip) === false) {
                $zip->addFile($image, $cheminDansZip);
            }
        }

        // Ajout du readme
        $zip->addFromString("Lisez-moi.txt", "Pack créé par la communauté "
            ."Le Gruppetto - PCMFrance\r\nhttps://legruppetto.fr/forum/\r\n"
            ."\r\nLe dossier Team est à placer à l'endroit suivant :\r\nC:\\"
            ."Program Files (x86)\\Steam\\steamapps\\common\\Pro Cycling "
            ."Manager 2018\\3D\\Cyclists\\Cloth\r\n\r\nSi vous souhaitez avoir "
            ."le mini-maillot en plus grande taille, contactez "
            ."contact.pcmfrance@gmail.com");

        $zip->close();
    }
}