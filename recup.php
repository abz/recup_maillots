<?php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Command\RecupMaillots;

$application = new Application();

$application->add(new RecupMaillots());
$application->run();